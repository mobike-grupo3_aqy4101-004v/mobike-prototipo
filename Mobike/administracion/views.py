from django.shortcuts import render, HttpResponse, redirect
from .models import *
from datetime import datetime, time


def inicio(request):
    return render(request, "administracion/inicio.html")


def registro(request):
    comunas = Comuna.objects.all()
    ciudades = Ciudad.objects.all()
    regiones = Region.objects.all()
    bancos = Banco.objects.all()
    c= request.POST.get('region')
    if request.method == 'GET':
        contexto = {'comunas': comunas, 'ciudades': ciudades, 'regiones': regiones, 'bancos': bancos, 'c': c}
        return render(request, "administracion/registro.html", contexto)

    elif request.method == 'POST':

        medioPago = MedioPago()
        medioPago.tarjeta = request.POST.get('tarjeta_banco')

        Banco_ob = request.POST.get('banco')
        banco_tarjeta = Banco.objects.get(pk = Banco_ob)
        medioPago.banco = banco_tarjeta
        medioPago.codigo_seguridad = request.POST.get('codigo_seguridad')
        medioPago.fecha_vencimiento = request.POST.get('fecha_tarjeta')
        medioPago.save()

        usuariog = Usuario()
        usuariog.rut_usuario = request.POST.get('rut')
        usuariog.nombres = request.POST.get('nombres') 
        usuariog.apellidos = request.POST.get('apellidos')
        usuariog.direccion = request.POST.get('direccion')

        region_us = Region.objects.get( region = request.POST.get('region'))
        usuariog.region = region_us

    
        ciudad_us = Ciudad.objects.get( ciudad = request.POST.get('ciudad'))
        usuariog.ciudad = ciudad_us


        comuna_us = Comuna.objects.get( comuna = request.POST.get('comuna'))
        usuariog.comuna = comuna_us

        usuariog.email = request.POST.get('email')
        usuariog.telefono = request.POST.get('telefono')
        contrauno = request.POST.get('pass')
        usuariog.contrass = contrauno
        usuariog.estado = 'Activo'
        usuariog.calificacion = 'Sin calificación'

        usuariog.save()

        usuarioM = UsuarioMobike()
        usuarioM.usuario = request.POST.get('rut')
        usuarioM.tarjetaPago = request.POST.get('tarjeta_banco')

        usuarioM.save()

        contexto = {'usuario' : usuariog,
            'comunas': comunas,
                        'ciudades': ciudades,
                        'regiones': regiones,
                        'bancos': bancos}
        return render(request, "administracion/registroexitoso.html", contexto)
            
            

        

    return render(request, "administracion/registro.html")

def registroexitoso(request):
    return render(request, 'administracion/registroexitoso.html')

def sesion(request):
    administrador = Administrador.objects.filter(usuario_admin= request.POST.get('user_admin')).exists()
    funcionario = Funcionario.objects.filter(usuario_funcionario= request.POST.get('user_admin')).exists()
    if request.method == 'POST' and administrador == False | funcionario == False:
        mensaje_error = "Credenciales ingresadas no son correctas"
        return render(request, "administracion/sesion.html", {'m': mensaje_error})

    if request.method == 'POST':
            ad = request.POST.get('user_admin')
            ps = request.POST.get('contrasenia_admin')

            if Administrador.objects.filter(usuario_admin= ad).exists() == True:
                admin_user = Administrador.objects.get(usuario_admin= ad)
                a = admin_user.usuario_admin
                c = admin_user.contrass
                n = admin_user.nombre_admin
                contexto = {'nombre_admin': n}
                if ad == a and ps == c:
                    return render(request, "administracion/cuenta_admin.html", contexto)
                else:
                    mensaje_error = "Credenciales ingresadas no son correctas"
                    return render(request, "administracion/sesion.html", {'m': mensaje_error})

            if Funcionario.objects.filter(usuario_funcionario= ad).exists() == True:
                admin_user = Funcionario.objects.get(usuario_funcionario = ad)
                a = admin_user.usuario_funcionario
                c = admin_user.contrass
                n = admin_user.nombre_funcionario
                contexto = {'nombre_admin': n}
                if ad == a and ps == c:
                    return render(request, "administracion/cuenta_func.html", contexto)
                else:
                    mensaje_error = "Credenciales ingresadas no son correctas"
                    return render(request, "administracion/sesion.html", {'m': mensaje_error})

    return render(request, "administracion/sesion.html")


def sesion2(request):
    usuario = Usuario.objects.filter(pk = request.POST.get('user')).exists()
    if request.method == 'POST' and usuario == False:
        mensaje_error = "Credenciales ingresadas no son correctas"
        return render(request, "administracion/sesion2.html", {'m': mensaje_error})     

    if request.method == 'POST':
        us = request.POST.get('user')
        uc = request.POST.get('contrasenia')
        user_registrado = Usuario.objects.get(rut_usuario = us)
        uss = user_registrado.rut_usuario
        ucc = user_registrado.contrass
        unn = user_registrado.nombres
        upp = user_registrado.apellidos
        ues = user_registrado.estado
        comunas = Comuna.objects.all()
        ciudades = Ciudad.objects.all()
        regiones = Region.objects.all()
        bancos = Banco.objects.all()
        comuna_mod = Comuna.objects.get(comuna = user_registrado.comuna)
        ciudad_mod = Ciudad.objects.get(ciudad= user_registrado.ciudad)
        region_mod = Region.objects.get(region= user_registrado.region) 
       

        contexto_usuario = {'ur': user_registrado, 'unn': unn, 'ciudades': ciudades, 'comunas': comunas,
        'regiones': regiones, 'bancos': bancos, 'comunamod': comuna_mod, 'ciudadmod': ciudad_mod, 'regionmod':region_mod}
        if us == uss and uc == ucc and ues == 'Activo':
            return render(request, "administracion/cuenta_usuario.html", contexto_usuario)
        

        else:
            mensaje_error = "Credenciales ingresadas no son correctas"
            return render(request, "administracion/sesion2.html", {'m': mensaje_error})            
    return render(request, "administracion/sesion2.html")
    


def cuenta(request):
    return render(request, 'administracion/cuenta_admin.html')

def cuentaUsuario(request, id_usuario):
    us = Usuario.objects.get(pk = id_usuario)
    return render(request, 'administracion/cuenta_usuario.html', {'ur': us})

def cuenta_funcionario(request):
    return render(request, 'administracion/cuenta_func.html')

def calificacion(request, id_usuario):
    us = Usuario.objects.get(pk = id_usuario)
    calificaciones = Calificacion.objects.all()
    if request.method == 'POST':
        us.calificacion = request.POST.get('calificacion')
        us.save()
        mensaje = "ok"
        return render(request, 'administracion/calificacion.html', {'usuario': us, 'calificaciones': calificaciones, 'mensaje' :mensaje})
        

    return render(request, 'administracion/calificacion.html', {'usuario': us, 'calificaciones': calificaciones})

def reportes(request):
    usuario = Usuario.objects.all()
    arriendos = Arriendo.objects.all()   
    cod_arriendo = ""
    if request.method == 'POST':
        if request.POST.get('buscar_codigo'):
            cod_arriendo = request.POST.get('buscar_codigo')
            arr = arriendos.filter(id_arriendo = cod_arriendo)
            if Arriendo.objects.filter(id_arriendo= cod_arriendo).exists() == True:    
                return render(request, 'administracion/reportes.html', {'todos': arr, 'arriendo': cod_arriendo, 'usuarios':usuario})
            else:
                m= "error"
                return render(request, 'administracion/reportes.html', {'todos': arriendos, 'arriendo': cod_arriendo, 'usuarios':usuario,'m':m})

         
        if request.POST.get('buscar_usuario'):
            cod_arriendo = request.POST.get('buscar_usuario')
            arr = arriendos.filter(usuario= cod_arriendo)
            mensaje = "ok"
            if Arriendo.objects.filter(usuario= cod_arriendo).exists() == True:
                return render(request, 'administracion/reportes.html', {'todos': arr, 'arriendo': cod_arriendo, 'mensaje' :mensaje, 'usuarios':usuario})
            else:
                m= "error"
                return render(request, 'administracion/reportes.html', {'todos': arriendos, 'arriendo': cod_arriendo, 'usuarios':usuario,'m':m})

        if request.POST.get('buscar_ubicacion_inicio'):
            cod_arriendo = request.POST.get('buscar_ubicacion_inicio')
            arr = arriendos.filter(ubicacion_origen= cod_arriendo)
            if Arriendo.objects.filter(ubicacion_origen= cod_arriendo).exists() == True:
                return render(request, 'administracion/reportes.html', {'todos': arr, 'arriendo': cod_arriendo, 'usuarios':usuario})
            else:
                m= "error"
                return render(request, 'administracion/reportes.html', {'todos': arriendos, 'arriendo': cod_arriendo, 'usuarios':usuario,'m':m})


        if request.POST.get(''):
            arriendos = Arriendo.objects.all()
            return render(request, 'administracion/reportes.html', {'todos': arriendos, 'usuarios':usuario})

    return render(request, 'administracion/reportes.html', {'todos':arriendos,'usuarios':usuario})


def Miperfil(request, id_usuario):
    user = Usuario.objects.get(pk = id_usuario)
    return render(request, 'administracion/miperfil.html', {'user': user})



def datos(request, id_usuario):
    comunas = Comuna.objects.all()
    ciudades = Ciudad.objects.all()
    regiones = Region.objects.all()

    usuario_mod = Usuario.objects.get(rut_usuario= id_usuario)
    comuna_mod = Comuna.objects.get(comuna = usuario_mod.comuna)
    ciudad_mod = Ciudad.objects.get(ciudad= usuario_mod.ciudad)
    region_mod = Region.objects.get(region= usuario_mod.region)

    if request.method == 'GET':
        return render(request, 'administracion/datos.html', {'usuario_mod': usuario_mod, 'ciudades': ciudades, 'comunas': comunas,
        'regiones': regiones, 'comunamod': comuna_mod, 'ciudadmod': ciudad_mod, 'regionmod':region_mod})
    elif request.method == 'POST':
        usuariog = Usuario()
        usuariog.rut_usuario = request.POST.get('rut')
        usuariog.nombres = request.POST.get('nombres') 
        usuariog.apellidos = request.POST.get('apellidos')
        usuariog.direccion = usuario_mod.direccion
        usuariog.region =  usuario_mod.region
        usuariog.ciudad = usuario_mod.ciudad
        usuariog.comuna = usuario_mod.comuna
        usuariog.email = request.POST.get('email')
        usuariog.telefono = request.POST.get('telefono')
        usuariog.contrass = usuario_mod.contrass
        usuariog.estado = 'Activo'
        usuariog.calificacion = "Sin calificacion"
        usuariog.save()

        contexto = {'usuario' : usuariog,
                    'comunas': comunas,
                    'ciudades': ciudades,
                    'regiones': regiones,}
        return render(request, "administracion/postmod.html", contexto)

def postmod(request):
    return render(request, 'administracion/postmod.html')

def direccion(request, id_usuario):
    comunas = Comuna.objects.all()
    ciudades = Ciudad.objects.all()
    regiones = Region.objects.all()

    usuario_mod = Usuario.objects.get(rut_usuario= id_usuario)
    comuna_mod = Comuna.objects.get(comuna = usuario_mod.comuna)
    ciudad_mod = Ciudad.objects.get(ciudad= usuario_mod.ciudad)
    region_mod = Region.objects.get(region= usuario_mod.region)
    if request.method == 'GET':
        return render(request, 'administracion/cambio_direccion.html', {'usuario_mod': usuario_mod, 'ciudades': ciudades, 'comunas': comunas,
        'regiones': regiones, 'comunamod': comuna_mod, 'ciudadmod': ciudad_mod, 'regionmod':region_mod})
    elif request.method == 'POST':
        usuariog = Usuario()
        usuariog.rut_usuario = usuario_mod.rut_usuario
        usuariog.nombres = usuario_mod.nombres
        usuariog.apellidos = usuario_mod.apellidos
        usuariog.direccion = request.POST.get('direccionc')

        region_us = Region.objects.get( region = request.POST.get('region'))
        usuariog.region = region_us

    
        ciudad_us = Ciudad.objects.get( ciudad = request.POST.get('ciudad'))
        usuariog.ciudad = ciudad_us


        comuna_us = Comuna.objects.get( comuna = request.POST.get('comuna'))
        usuariog.comuna = comuna_us

        usuariog.email = usuario_mod.email
        usuariog.telefono = usuario_mod.telefono
        usuariog.contrass = usuario_mod.contrass
        usuariog.estado = 'Activo'
        usuariog.calificacion = "Sin calificacion"
        usuariog.save()

        contexto = {'usuario' : usuariog,
                    'comunas': comunas,
                    'ciudades': ciudades,
                    'regiones': regiones,}
        return render(request, "administracion/postmod.html", contexto)


    return render(request, 'administracion/cambio_direccion.html', )


def cambiopass(request, id_usuario):
    usuario_mod = Usuario.objects.get(rut_usuario= id_usuario)
    if request.method == 'POST':
        usuariog = Usuario()
        usuariog.rut_usuario = usuario_mod.rut_usuario
        usuariog.nombres = usuario_mod.nombres
        usuariog.apellidos = usuario_mod.apellidos
        usuariog.direccion = usuario_mod.direccion
        usuariog.region =  usuario_mod.region
        usuariog.ciudad = usuario_mod.ciudad
        usuariog.comuna = usuario_mod.comuna
        usuariog.email = usuario_mod.email
        usuariog.telefono = usuario_mod.telefono
        usuariog.contrass = request.POST.get('passc1')
        confirmacion = request.POST.get('passc2')
        usuariog.estado = 'Activo'
        usuariog.calificacion = "Sin calificacion"
        if  usuariog.contrass == confirmacion:
            usuariog.save()
            mensaje_exito = "CAMBIO REALIZADO CON EXITO"
            return render(request, 'administracion/cambio_contrass.html',  {'usuario_mod': usuariog, 'o': mensaje_exito})
        else:
            mensaje_error = "LAS CONTRASEÑAS DEBEN SER IGUALES"
            return render(request, 'administracion/cambio_contrass.html',  {'usuario_mod': usuario_mod, 'm': mensaje_error})
        
    return render(request, 'administracion/cambio_contrass.html',  {'usuario_mod': usuario_mod})

def tarjeta_eliminar(request, id_usuario, id_tarjeta):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    mediopago = MedioPago.objects.get(tarjeta = id_tarjeta)
    tarjetas = MedioPago.objects.all()
    numero_tarjeta = mediopago.tarjeta
    cod_tarjeta = mediopago.codigo_seguridad
    fecha_tarjeta = mediopago.fecha_vencimiento
    banco = mediopago.banco_id
    nombre_banco = Banco.objects.get(pk = banco)
    if request.method == 'POST':
        MedioPago.objects.filter(tarjeta= id_tarjeta).delete()
        UsuarioMobike.objects.filter(tarjetaPago= id_tarjeta).delete()
        m = "La tarjeta ha sido borrada exitosamente"
        return render(request, 'administracion/t_ya_eliminada.html', {'usuario': usuario, 'banco': nombre_banco, 'mediopago': numero_tarjeta,
        'codigo_tarjeta':cod_tarjeta, 'fecha_tarjeta': fecha_tarjeta, 'm':m})
    return render(request, 'administracion/tarjeta_eliminar.html', {'usuario': usuario, 'banco': nombre_banco, 'mediopago': numero_tarjeta,
    'codigo_tarjeta':cod_tarjeta, 'fecha_tarjeta': fecha_tarjeta, 'tarjetas': tarjetas})
        
       
def t_ya_eliminada(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    return render(request, 'administracion/t_ya_eliminada.html', {'usuario': usuario})

def agregartarjeta(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    bancos = Banco.objects.all()
    if request.method == 'POST':
        medioPago = MedioPago()

        medioPago.tarjeta = request.POST.get('tarjeta_banco_nueva')
        Banco_ob = request.POST.get('banco_nuevo')
        banco_tarjeta = Banco.objects.get(pk = Banco_ob)
        medioPago.banco = banco_tarjeta
        medioPago.codigo_seguridad =  request.POST.get('codigo_seguridad_nuevo')
        medioPago.fecha_vencimiento = request.POST.get('fecha_tarjeta_nueva')
        

        usuarioM = UsuarioMobike()
        usuarioM.usuario = usuario.rut_usuario
        usuarioM.tarjetaPago = request.POST.get('tarjeta_banco_nueva')

        if MedioPago.objects.filter(tarjeta= request.POST.get('tarjeta_banco_nueva')).exists():
            r = "YA ESTA LA TARJETA"
            return render(request, 'administracion/agregartarjeta.html', {'usuario': usuario, 'bancos': bancos, 'r':r})
        else:
            medioPago.save()
            usuarioM.save()

        return render(request, 'administracion/agregartarjeta.html', {'usuario': usuario, 'bancos': bancos, 'medio_pago' : medioPago})
        

    return render(request, 'administracion/agregartarjeta.html', {'usuario': usuario, 'bancos': bancos})

def mediospago(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    tarjetas = UsuarioMobike.objects.filter(usuario=  id_usuario)
    tar = ""
    if request.method == 'POST':
        if request.POST.get('buscartarjeta'):
            tar = request.POST.get('buscartarjeta')
            tars = tarjetas.filter(tarjetaPago = tar)
            if MedioPago.objects.filter(tarjeta = tar).exists() == True:    
                return render(request, 'administracion/mediospago.html', {'tarjetas': tars, 'usuario' : usuario})
            else:
                m= "error"
                tarjetas = UsuarioMobike.objects.filter(usuario=  id_usuario)
                return render(request, 'administracion/mediospago.html', {'tarjetas': tarjetas, 'usuario' : usuario,'m':m})
            
        if request.POST.get(''):
            tarjetas = UsuarioMobike.objects.filter(usuario=  id_usuario)
            return render(request, 'administracion/mediospago.html', {'tarjetas': tarjetas})
    return render(request, "administracion/mediospago.html", {'tarjetas': tarjetas,  'usuario' : usuario})   




def listar(request):
    administrador = Administrador.objects.filter(usuario_admin= request.POST.get('user')).exists()
    usuarios = Usuario.objects.all()
    rut = ""
    if request.method == 'POST':
        if request.POST.get('buscar'):
            rut = request.POST.get('buscar')
            us = usuarios.filter(rut_usuario= rut)
            if Usuario.objects.filter(rut_usuario= rut).exists() == True:    
                return render(request, 'administracion/listar.html', {'todos': us, 'rut': rut})
            else:
                m= "error"
                usuarios = Usuario.objects.all()
                return render(request, 'administracion/listar.html', {'todos': usuarios, 'm': m })

        if request.POST.get(''):
            usuarios = Usuario.objects.all()
            return render(request, 'administracion/listar.html', {'todos': usuarios})
            
    return render(request, 'administracion/listar.html', {'todos': usuarios})




def arriendo(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    estaciones = Estacion.objects.all()
    e = ""
    if request.method== 'POST':
        if request.POST.get('buscarestacion'):
            e = request.POST.get('buscarestacion')
            es = estaciones.filter(ubicacion = e)
            if Estacion.objects.filter(ubicacion = e).exists() == True:
                return render(request, 'administracion/arriendo.html', {'estaciones': es, 'usuario': usuario})
            else:
                m= "error"
                return render(request, 'administracion/arriendo.html', {'estaciones': estaciones, 'usuario' : usuario,'m':m})

        if request.POST.get(''):
            estaciones = Estacion.objects.all()
            return render(request, 'administracion/arriendo.html',{'estaciones': estaciones, 'usuario': usuario})
    return render(request, 'administracion/arriendo.html', {'estaciones': estaciones, 'usuario': usuario})




def bicis(request, id_e, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    bicis = Bicicleta.objects.filter(estacion_bicicleta_id = id_e)
    contexto = {'bc': bicis, 'usuario': usuario, 'id_e': id_e }
    return render(request, 'administracion/bicis.html', contexto)



def codigo(request, cod, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    bici = Bicicleta.objects.get(numero_bicicleta=cod)
    cod = bici.codigo_bicicleta
    contexto = {'usuario': usuario , 'bici': bici}
    if request.method == 'POST':
        if request.POST.get('codigo_des') == cod:
            contexto = {'usuario': usuario , 'cod': cod, 'bici': bici}
            return render(request, 'administracion/ingreso_codigo.html', contexto)
        else:
            m = "EL CODIGO NO COINCIDE, INTENTE DE NUEVO"
            return render(request, 'administracion/ingreso_codigo.html' ,{'usuario': usuario , 'bici': bici , 'm': m})

    return render(request, 'administracion/ingreso_codigo.html', contexto)

    

def encurso(request, cod, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    e = Estacion.objects.get(id = cod)
    ed = "Los leones"
    estacion = e.ubicacion
    if request.method == 'POST':
        arriendo = Arriendo()
        arriendo.id_arriendo = request.POST.get('cod_arriendo')
        arriendo.ubicacion_origen =  estacion
        arriendo.ubicacion_termino = ed
        arriendo.hora_inicio = request.POST.get('hora_inicio')
        arriendo.hora_termino= request.POST.get('hora_termino')
        arriendo.costo = request.POST.get('costo')
        arriendo.usuario_id = usuario.rut_usuario
        arriendo.save()
        return render(request, 'administracion/arriendo_realizado.html', {'usuario': usuario})

   
    return render(request, 'administracion/arriendo_encurso.html', {'usuario': usuario})
         

def arriendo_realizado(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    contexto = {'usuario': usuario}
    return render(request, 'administracion/arriendo_realizado.html', contexto )

def mistransacciones(request, id_usuario):
    usuario = Usuario.objects.get(rut_usuario= id_usuario)
    lista_arriendo = Arriendo.objects.filter(usuario = id_usuario)
    codigo_arriendo = ""
    if request.method == 'POST':
        if request.POST.get('buscartransaccion'):
            codigo_arriendo = request.POST.get('buscartransaccion')
            ars = lista_arriendo.filter(id_arriendo=codigo_arriendo)
            return render(request, 'administracion/mistransacciones.html',{'recorridos': ars, 'usuario' : usuario})
        if request.POST.get(''):
            lista_arriendo = Arriendo.objects.filter(usuario = id_usuario)
            return render(request, 'administracion/mistransacciones.html',{'recorridos': lista_arriendo, 'usuario' : usuario})
    return render(request, 'administracion/mistransacciones.html', {'recorridos': lista_arriendo, 'usuario' : usuario})



def editar(request, id_usuario):
    comunas = Comuna.objects.all()
    ciudades = Ciudad.objects.all()
    regiones = Region.objects.all()
    bancos = Banco.objects.all()
    usuario_mod = Usuario.objects.get(rut_usuario= id_usuario)
    comuna_mod = Comuna.objects.get(comuna = usuario_mod.comuna)
    ciudad_mod = Ciudad.objects.get(ciudad= usuario_mod.ciudad)
    region_mod = Region.objects.get(region= usuario_mod.region) 
    if request.method == 'GET':
        return render(request, 'administracion/editar.html', {'usuario_mod': usuario_mod, 'ciudades': ciudades, 'comunas': comunas,
        'regiones': regiones, 'bancos': bancos, 'comunamod': comuna_mod, 'ciudadmod': ciudad_mod, 'regionmod':region_mod})
    elif request.method == 'POST':
        usuariog = Usuario()
        usuariog.rut_usuario = request.POST.get('rut')
        usuario_a = Usuario.objects.get( rut_usuario =  usuariog.rut_usuario )
        usuariog.nombres = request.POST.get('nombres') 
        usuariog.apellidos = request.POST.get('apellidos')
        usuariog.direccion = request.POST.get('direccion')

        region_us = Region.objects.get( region = request.POST.get('region'))
        usuariog.region = region_us

    
        ciudad_us = Ciudad.objects.get( ciudad = request.POST.get('ciudad'))
        usuariog.ciudad = ciudad_us


        comuna_us = Comuna.objects.get( comuna = request.POST.get('comuna'))
        usuariog.comuna = comuna_us

        usuariog.email = request.POST.get('email')
        usuariog.telefono = request.POST.get('telefono')
        usuariog.contrass = usuario_a.contrass
        usuariog.calificacion = usuario_mod.calificacion
        usuariog.estado = usuario_a.estado
        usuariog.save()

        

        contexto = {'usuario' : usuariog,
                    'comunas': comunas,
                    'ciudades': ciudades,
                    'regiones': regiones,
                    'bancos': bancos }
        return render(request, 'administracion/posactuAlizacion.html', contexto)

  
    
def actualizar(request, id_usuario):
    return render(request, "administracion/posactuAlizacion.html")


def bloquear(request, id_usuario):
    usuario_mod = Usuario.objects.get(rut_usuario= id_usuario)
    if request.method == 'POST':
        usuariob = Usuario()
        usuariob.estado =  request.POST.get('seleccion')
        usuario_mod.estado = usuariob.estado
        usuario_mod.save()
        c = request.POST.get('seleccion')
        cd = c
        return render(request, 'administracion/posbloqueo.html', {'usuario_bloq': usuario_mod, 'cd': cd})
    return render(request, 'administracion/bloquear.html', {'usuario_bloq': usuario_mod})
