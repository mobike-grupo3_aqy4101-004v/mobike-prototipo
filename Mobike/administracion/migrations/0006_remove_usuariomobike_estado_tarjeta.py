# Generated by Django 3.1.2 on 2020-12-07 03:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administracion', '0005_usuariomobike_estado_tarjeta'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usuariomobike',
            name='estado_tarjeta',
        ),
    ]
