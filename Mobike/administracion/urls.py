from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static



app_name = 'administracion'

urlpatterns = [ 
    path('', views.inicio, name="inicio"),
    path('registro/', views.registro, name="registro"),
    path('sesion/', views.sesion,  name="sesion"),
    path('sesion_usuario/', views.sesion2,  name="sesion2"),
    path('cuenta_admin/', views.cuenta, name="cuenta_admin"),
    path('cuenta_usuario/<str:id_usuario>', views.cuentaUsuario, name="cuenta_usuario"),
    path('cuenta_funcionario/', views.cuenta_funcionario, name="cuenta_funcionario"),
    path('calificacion/<str:id_usuario>', views.calificacion, name="calificacion"),
    path('reportes/', views.reportes, name="reportes"),
    path('miperfil/<str:id_usuario>/', views.Miperfil, name="perfil"),
    path('misdatos/<str:id_usuario>', views.datos, name="datos"),
    path('listar/', views.listar, name="listar"),
    path('editar_usuario/<str:id_usuario>', views.editar, name="editar"),
    path('bloquear_usuario/<str:id_usuario>', views.bloquear, name="bloquear"),
    path('usuario_ya_actualizado/<str:id_usuario>', views.actualizar, name="ya_actualizado"),
    path('usuario_ya_bloqueado/', views.bloquear, name="ya_bloqueado"),
    path('registro/registro_exitoso/', views.registroexitoso, name="ya_registrado"),
    path('yamodificado/', views.datos, name="ya_mod"),
    path('arriendo/<str:id_usuario>', views.arriendo, name="arriendo"),
    path('bici_disponible/<int:id_e>/<str:id_usuario>', views.bicis, name="bicis"),
    path('codigo_verificacion/<str:cod>/<str:id_usuario>', views.codigo, name="ingreso_codigo"),
    path('arriendo_encurso/<str:cod>/<str:id_usuario>', views.encurso, name="encurso"),
    path('arriendo_realizado/<str:id_usuario>', views.arriendo_realizado, name="arriendo_realizado"),
    path('cambio_direccion/<str:id_usuario>', views.direccion, name="direccion"),
    path('cambio_pass/<str:id_usuario>', views.cambiopass, name="cambiopass"),
    path('mistransacciones/<str:id_usuario>', views.mistransacciones, name="mistransacciones"),
    path('mismediopago/<str:id_usuario>', views.mediospago, name="mediospago"),
    path('tarjeta/<str:id_usuario>/<str:id_tarjeta>', views.tarjeta_eliminar, name="tarjeta"),
    path('nuevatarjeta/<str:id_usuario>', views.agregartarjeta, name="agregartarjeta"),
    path('t_eliminada/<str:id_usuario>', views.t_ya_eliminada, name="ya_eliminada"),
    
    
]