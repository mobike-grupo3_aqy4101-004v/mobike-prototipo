from django.contrib import admin
from .models import *

admin.site.register(Comuna)
admin.site.register(Ciudad)
admin.site.register(Region)
admin.site.register(Banco)
admin.site.register(Administrador)
admin.site.register(Usuario)
admin.site.register(UsuarioMobike)
admin.site.register(MedioPago)
admin.site.register(Funcionario)
admin.site.register(Estacion)
admin.site.register(Bicicleta)
admin.site.register(Calificacion)
admin.site.register(Arriendo)



