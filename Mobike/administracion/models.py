from django.db import models
from datetime import datetime, time
now= time()
Hora = now.hour
minuto = now.minute
segundo = now.second
class Region(models.Model):
    region= models.CharField(max_length=100)
    def __str__(self):
        return self.region

class Ciudad(models.Model):
    ciudad= models.CharField(max_length=100)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    def __str__(self):
        return self.ciudad

class Comuna(models.Model):
    comuna = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)

    def __str__(self):
        return self.comuna

class Banco(models.Model):
    nombre_banco = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre_banco


class MedioPago(models.Model):
    tarjeta = models.CharField(max_length=25, primary_key=True)
    codigo_seguridad = models.CharField(max_length=3)
    fecha_vencimiento = models.CharField(max_length=3)
    banco = models.ForeignKey(Banco, on_delete=models.CASCADE)
    

class Usuario(models.Model):
    rut_usuario= models.CharField(max_length=20, primary_key=True)
    nombres= models.CharField(max_length=100)
    apellidos= models.CharField(max_length=100)
    direccion= models.CharField(max_length=100)
    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    ciudad= models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    region= models.ForeignKey(Region, on_delete=models.CASCADE)
    email= models.EmailField(max_length=254)
    telefono= models.CharField(max_length=100)
    contrass= models.CharField(max_length=20)
    estado = models.CharField(max_length=20)
    calificacion = models.CharField(max_length=200)
    

    def __str__(self):
        return self.nombres

class Administrador(models.Model):
    rut_administrador = models.CharField(max_length=20, primary_key=True)
    nombre_admin = models.CharField(max_length=100)
    usuario_admin = models.CharField(max_length=100)
    contrass= models.CharField(max_length=20)

    def __str__(self):
        return self.nombre_admin

class UsuarioMobike(models.Model):
    usuario = models.CharField(max_length=20)
    tarjetaPago = models.CharField(max_length=25, primary_key=True )

class Estacion(models.Model):
    ubicacion = models.CharField(max_length=20)
    def __str__(self):
        return self.ubicacion

class Funcionario(models.Model):
    rut_funcionario = models.CharField(max_length=20, primary_key=True)
    nombre_funcionario = models.CharField(max_length=100)
    usuario_funcionario = models.CharField(max_length=100)
    contrass = models.CharField(max_length=20)

class Arriendo(models.Model):
    id_arriendo = models.CharField(max_length=200, primary_key=True)
    ubicacion_origen = models.CharField(max_length=200)
    ubicacion_termino = models.CharField(max_length= 200)
    hora_inicio = models.CharField(max_length=30, default=str(Hora)+":"+str(minuto)+":"+str(segundo), editable=False)
    hora_termino = models.CharField(max_length=30, default=str(now.hour + 1)+":"+str(now.minute)+":"+str(now.second), editable=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    costo = models.CharField(max_length=30)


class Bicicleta(models.Model):
    numero_bicicleta = models.CharField(max_length=10)
    estado_bicicleta = models.CharField(max_length=10)
    codigo_bicicleta = models.CharField(max_length=10)
    estacion_bicicleta = models.ForeignKey(Estacion, on_delete=models.CASCADE)


class Calificacion(models.Model):
    nombre_calificacion = models.CharField(max_length=50)





